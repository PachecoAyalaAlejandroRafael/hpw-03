titulo=[
  "posicion","no_control","nombre"
  ];

dato=[
  ["1","1010","aaaaa"],
  ["2","1111","bbbb"],
  ["3","1212","cccccc"]
];

f(titulo,dato);

function f(titulo,dato)
{
  var tabla=document.createElement("table");
  var cabecera=document.createElement("thead");
  var titulos=document.createElement("tr");
  for(var i=0;i<titulo.length;i++)
  {
    var t1=titulo[i];
    var elemento=document.createElement("th");
    var contenido=document.createTextNode(t1);
    elemento.appendChild(contenido);
    titulos.appendChild(elemento);
  }
  cabecera.appendChild(titulos);
  tabla.appendChild(cabecera);
  var cuerpo=document.createElement("tbody");
  for(var i=0;i<dato.length;i++)
  {
    var datos=document.createElement("tr");
    for(var j=0;j<dato[i].length;j++)
    {
      var dato1=dato[i][j];
      var elemento1=document.createElement("td");
      var contenido1=document.createTextNode(dato1);
      elemento1.appendChild(contenido1);
      datos.appendChild(elemento1);
    }
    cuerpo.appendChild(datos);
  }
  tabla.appendChild(cuerpo);
  document.body.appendChild(tabla);
}